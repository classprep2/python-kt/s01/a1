name = "Christopher Malinao"
age = 24
occupation = "Programming Instructor"
movie = "Avengers"
rate = 99.8

print("I am {}, and I am {} years old, I work as a {}, and my rating for {} is {}.".format(name, age, occupation, movie, rate))

num1 = 10
num2 = 20
num3 = 30
isLessThan = True

print("Product of num1 and num2: {}".format(num1*num2))
if num1 < num3 :
	isLessThan = True

else:
	isLessThan = False

print("Is num1 less than num3: {}".format(isLessThan))

print("Sum of num1 and num2: {}".format(num1+num2))
